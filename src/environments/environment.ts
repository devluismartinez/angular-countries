// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDjNO7nH-sjxw3w3nqqaT6Q6_-Jjxw09PY',
    authDomain: 'angularmaps-b05e1.firebaseapp.com',
    databaseURL: 'https://angularmaps-b05e1.firebaseio.com',
    projectId: 'angularmaps-b05e1',
    storageBucket: 'angularmaps-b05e1.appspot.com',
    messagingSenderId: '381821598343'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.4

