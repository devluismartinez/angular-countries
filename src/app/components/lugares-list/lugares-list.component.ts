import { Component, OnInit, Input } from '@angular/core';
import { Lugar } from 'src/app/models/lugar.model';

@Component({
  selector: 'app-lugares-list',
  templateUrl: './lugares-list.component.html',
  styleUrls: ['./lugares-list.component.css']
})
export class LugaresListComponent implements OnInit {

  @Input() lugares: Array<Lugar>;

  constructor() { }

  ngOnInit() {
    
  }

}
