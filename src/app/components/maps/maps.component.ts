import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css']
})
export class MapsComponent implements OnInit {

  @Input() lat: number;
  @Input() lng: number;

  constructor() { 
    console.log(this.lat);
  }

  ngOnInit() {
  }

}
