import { Injectable } from '@angular/core';
import { Continente } from 'src/app/models/continente.model';

@Injectable({
  providedIn: 'root'
})
export class ContinenteService {

  continentes: Array<Continente> = [
    { 
      id:1,
      nombre: 'Africa',
      stringCodeRegion: 'africa'
    },
    { 
      id:2,
      nombre: 'America',
      stringCodeRegion: 'americas'
    },
    { 
      id:3,
      nombre: 'Asia',
      stringCodeRegion: 'asia'
    },
    { 
      id:4,
      nombre: 'Europe',
      stringCodeRegion: 'europe'
    },
    { 
      id:5,
      nombre: 'Oceania',
      stringCodeRegion: 'oceania'
    },
  ]


  constructor() { }

  getContinentes(): Array<Continente>{
    return this.continentes;
  }
}
