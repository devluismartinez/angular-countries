import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private _endPoint: string;
  constructor() { 
    this._endPoint = 'https://restcountries.eu/rest/v2/'
  }

  getQuery(query:string){
    return (this._endPoint + query);
  }
}
