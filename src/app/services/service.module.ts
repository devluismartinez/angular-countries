import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContinenteService, PaisService } from './service.libraries';

@NgModule({
    declarations: [],
    imports: [
      CommonModule
    ],
    providers: [
        ContinenteService,
        PaisService
    ]
})

export class ServiceModule {}