import { Injectable } from '@angular/core';
import { Lugar } from 'src/app/models/lugar.model';

@Injectable({
  providedIn: 'root'
})
export class LugaresService {

  lugares: Array<Lugar> = [
    {
      id:1, 
      nombre:"Torre Orquideas",
      latitud: -12.0920442,
      longitud: -77.0284625
    },
    {
      id:2, 
      nombre:"Torre Pacífico",
      latitud: -12.0965302,
      longitud: -77.0296428
    },
    {
      id:3, 
      nombre:"The Westin Lima Hotel & Convention Center",
      latitud: -12.091117,
      longitud: -77.025848
    }
  ];

  constructor() { }

  getLugares(): Array<Lugar>{
    return this.lugares;
  }

  getLugaresById(id:number): Lugar{
    return this.lugares.filter( (lugares) => lugares.id === id)[0];
    
  }
  
}
