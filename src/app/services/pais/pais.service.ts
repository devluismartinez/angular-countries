import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, ObservableInput } from 'rxjs';
import { map } from 'rxjs/operators';
import { Pais } from 'src/app/models/pais.model';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class PaisService {

  constructor(private httpClient: HttpClient,
              private httpEndPoint: HttpService) { }

  getPaises():Observable<Array<Pais>>{
    return this.httpClient.get(this.httpEndPoint.getQuery('all'))
      .pipe(
        map( (resp: Array<Pais>) => resp)
      );
  }

  getPaisesByContinent(region: string): Observable<Array<Pais>>{
    return this.httpClient.get(this.httpEndPoint.getQuery('region/'+region))
      .pipe(
        map( (resp:Array<Pais>) => resp)
      );
  }

  getDetallePais(isoCode:string): Observable<Pais>{
    return this.httpClient.get(this.httpEndPoint.getQuery('alpha/'+isoCode))
    .pipe(
      map( (resp:Pais) => resp)
    );;
  }

  getPaisesByName(name:string):Observable<Array<Pais>>{
    return this.httpClient.get(this.httpEndPoint.getQuery('name/'+name))
    .pipe(
      map( (resp:Array<Pais>) => resp)
    )
  }

}

