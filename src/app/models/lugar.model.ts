import { ILugar } from '../intefaces/lugar.interface';

export class Lugar implements ILugar{
    public id: number;
    public nombre: string;
    public latitud: number;
    public longitud: number;
}