import { IContinente } from '../intefaces/continente.interface';

export class Continente implements IContinente{

    public id: number;
    public nombre: string;
    public stringCodeRegion: string;
}