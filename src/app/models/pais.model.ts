import { IPais } from '../intefaces/pais.interface';

export class Pais implements IPais{

    public isoCode: string;
    public name: string;
    public flag: string;
    public alpha3Code: string;

}

