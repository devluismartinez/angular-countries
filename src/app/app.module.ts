import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { environment } from '../environments/environment';
import { HttpClientModule } from '@angular/common/http';

//Routing
import { ROUTING } from './app.routing';

//google maps
import { AgmCoreModule } from '@agm/core';

//firebase modules
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';

//services
import { ServiceModule } from './services/service.module';

//pages
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { PaisComponent } from './pages/pais/pais.component';
import { PaisDetalleComponent } from './pages/pais/pais-detalle/pais-detalle.component';

//components
import { NavbarComponent } from './shared/navbar/navbar.component';
import { MapsComponent } from './components/maps/maps.component';
import { LoadingComponent } from './shared/loading/loading.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    MapsComponent,
    HomeComponent,
    PaisComponent,
    PaisDetalleComponent,
    LoadingComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ServiceModule,
    ROUTING,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyA8uxyqjaIV2kG_XNwLESzYR38NkiIAefo'
    }),
    AngularFireModule.initializeApp(environment.firebase, 'my-app-name'), // imports firebase/app needed for everything
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule // imports firebase/storage only needed for storage features
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
