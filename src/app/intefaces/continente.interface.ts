export interface IContinente{

    id: number;
    nombre: string;
    stringCodeRegion: string
}