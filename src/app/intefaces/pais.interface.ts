export interface IPais{
    isoCode: string;
    name: string;
    flag: string;
    alpha3Code: string;
}