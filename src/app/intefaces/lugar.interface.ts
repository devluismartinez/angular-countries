export interface ILugar {
    id: number;
    nombre: string;
    latitud: number;
    longitud: number;
}