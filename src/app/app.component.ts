import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {

  title = 'platzisquare';
  listo = true;

  counter: number;
  nombre: string;

  lat: number = -12.0919562;
  lng: number = -77.0306584;

  constructor(){

    this.counter = 0;
    // setTimeout(() => {
    //   this.listo = true;
    // }, 3000);
  }

  counterIncrement(): void{
    this.counter++;
  }

}
