import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { LugaresComponent } from './pages/lugares/lugares.component';
import { LugaresDetalleComponent } from './pages/lugares/lugares-detalle.component';
import { PaisComponent } from './pages/pais/pais.component';
import { PaisDetalleComponent } from './pages/pais/pais-detalle/pais-detalle.component';

const ROUTES: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent},
    { path: 'country', component: PaisComponent},
    { path: 'country/detail/:isoCode', component: PaisDetalleComponent}
];

export const ROUTING = RouterModule.forRoot(ROUTES);