import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent, PaisComponent, PaisDetalleComponent } from './pages.index';

@NgModule({
    declarations:[
    ],
    imports: [
        CommonModule
    ],
    providers:[
        HomeComponent,
        PaisComponent,
        PaisDetalleComponent
    ]
})

export class PagesModule {}