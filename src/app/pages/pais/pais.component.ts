import { Component, OnInit } from '@angular/core';
import { PaisService } from 'src/app/services/pais/pais.service';
import { Pais } from 'src/app/models/pais.model';
import { Continente } from 'src/app/models/continente.model';
import { ContinenteService } from 'src/app/services/continente/continente.service';

@Component({
  selector: 'app-pais',
  templateUrl: './pais.component.html',
  styleUrls: ['./pais.component.css']
})
export class PaisComponent implements OnInit {

  paises: Array<Pais>;
  continentes: Array<Continente>;
  loading:boolean;
  querySearch: string;

  constructor(private paisService: PaisService,
              private continenteService: ContinenteService) { 
    this.getContinentes();
  }

  ngOnInit() {

  }

  getPaisesByRegion(codeContinent:string) : void{
    this.loading = true;
    this.paisService.getPaisesByContinent(codeContinent).subscribe( (resp) => {
      this.paises = resp;
      console.log(this.paises);
      this.loading = false;
    });
  }

  getContinentes(){
    this.continentes = this.continenteService.getContinentes();
  }

  searchPais(){
    this.loading = true;
    this.paisService.getPaisesByName(this.querySearch).subscribe( (resp) => {
      this.paises = resp;
      this.loading = false;
    });
  }
}
