import { Component, OnInit } from '@angular/core';
import { PaisService } from 'src/app/services/pais/pais.service';
import { Pais } from 'src/app/models/pais.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pais-detalle',
  templateUrl: './pais-detalle.component.html',
  styleUrls: ['./pais-detalle.component.css']
})
export class PaisDetalleComponent implements OnInit {

  isoCode: string;
  pais: Pais;
  constructor( private paisService: PaisService,
                private activatedRoute: ActivatedRoute) { 

    this.isoCode = this.activatedRoute.snapshot.params['isoCode'];
  }

  ngOnInit() {
    this.getDetallePais();
  }

  getDetallePais(){
    this.paisService.getDetallePais(this.isoCode).subscribe( (data) => {
      this.pais = data;
      console.log(this.pais);
    });
  }

}
