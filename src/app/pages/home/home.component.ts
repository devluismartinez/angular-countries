import { Component, OnInit } from '@angular/core';
import { PaisService } from 'src/app/services/pais/pais.service';
import { Pais } from 'src/app/models/pais.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private paisService: PaisService) { }

  paises: Array<Pais>;
  ngOnInit() {
  }

  getPaises() {
    this.paisService.getPaises().subscribe( (data) => {
      this.paises = data
    })
  }

}
