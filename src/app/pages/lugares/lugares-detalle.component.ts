import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LugaresService } from 'src/app/services/lugares/lugares.service';
import { Lugar } from 'src/app/models/lugar.model';

@Component({
  selector: 'app-lugares-detalle',
  templateUrl: './lugares-detalle.component.html',
  styleUrls: ['./lugares-detalle.component.css']
})
export class LugaresDetalleComponent implements OnInit {

  id: number;
  lugar: Lugar;
  constructor(private activatedRoute: ActivatedRoute,
              private lugaresService: LugaresService) { }

  ngOnInit() {
    this.id = +this.activatedRoute.snapshot.params['id'];
    this.getLugaresById();
  }

  getLugaresById(){
    this.lugar = this.lugaresService.getLugaresById(this.id);
  }

}
