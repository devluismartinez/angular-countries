import { Component, OnInit } from '@angular/core';
import { LugaresService } from 'src/app/services/lugares/lugares.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Lugar } from 'src/app/models/lugar.model';

@Component({
  selector: 'app-lugares',
  templateUrl: './lugares.component.html',
  styleUrls: ['./lugares.component.css']
})
export class LugaresComponent implements OnInit {
  
  lugares: Array<Lugar>;
  constructor( private lugaresService: LugaresService) { }

  ngOnInit() {
    this.getLugares();
  }

  getLugares(){
    this.lugares = this.lugaresService.getLugares();
  }
}
